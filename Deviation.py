from math import *

def Deviation(distance, force, angle_degrees):
    # Conversion de l'angle de degrés à radians
    angle_radians = radians(angle_degrees)
    
    # Calcul des composantes du déplacement en x et y
    deplacement_x = force * cos(angle_radians)
    deplacement_y = force * sin(angle_radians)
    
    # Calcul de la déviation
    deviation_x = int(deplacement_x * (distance / 100))  # Hypothèse de déviation proportionnelle à la distance
    deviation_y = int(deplacement_y * (distance / 100))
    
    return deviation_x, deviation_y

