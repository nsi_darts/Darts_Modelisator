#Importation des modules nécessaires
from pygame import *
from sys import *
from Points_Cible import Points_Cible
from random import *
from Deviation import Deviation

#Création de la fenêtre Pygame
init()
fenetre_hauteur=500
fenetre_largeur=503
fenetre_flechettes=display.set_mode((fenetre_hauteur,fenetre_largeur))
display.set_caption("Jeu De Flechettes")

#Importation de la cible et de l'îcone du programme
cible=image.load('./images/Cible.png')
cible_arriere_plan=cible.convert()
icone=image.load('./images/Cible_Icone.png')
display.set_icon(icone)

#Définition d'une boucle while pour tant que la fenêtre est ouverte !
while True:
    for evenement in event.get():
        if evenement.type == QUIT:
            with open('resultats.txt', 'w') as file:
                file.write('')
            quit()
            exit()
        if evenement.type== MOUSEBUTTONDOWN: #Si il y a eu interaction avec la souris (la roulette sur la souris fonctionne à voir pour modifier)
            distance_cible =int(input("Quel est la distance entre vosu et la cible (Entre 1000 et 4000) ?"))  # Distance entre la fléchette et la cible (en unités arbitraires)
            force_flechette = int(input("Quel est votre exercement de votre force sur la fléchette ? (Entre 1 et 15)")) # Force de la fléchette (en unités arbitraires)
            angle = int(input("Quel est l'angle de votre visé en degré (entre 1 et 90) ?"))
            x,y=mouse.get_pos() #Récupération des coordonnées de la souris
            couleur = cible.get_at((x, y)) #Récupération de la couleur à ces coordonnées !
            deviation_x,deviation_y=Deviation(distance_cible,force_flechette,angle)
            if deviation_x>fenetre_hauteur or deviation_y>fenetre_largeur:
                 print("Vous êtes en dehors de la fenêtre. Réessayer")
            else:
                nouvelle_couleur=cible.get_at((deviation_x,deviation_y))
                message2=f"******************************NOUVEAUX RESULTATS******************************\nLa fléchette a touché la cible aux coordonnées : {deviation_x,deviation_y}\nla nouvelle couleur sous forme RVB que la fléchette a touché est : {nouvelle_couleur}\nLes points obtenus au final sont : {Points_Cible(nouvelle_couleur)}"
                print(message2)
            message=f"******************************RESULTATS LANCER******************************\nVous avez visé avec votre souris les coordonnées : {x,y}.\nLa couleur sous forme RVB que vous avez visé est : {couleur}.\nLes points obtenus par votre visée sont : {Points_Cible(couleur)}"
            print(message)
            print('****************************************************************************')
            with open('resultats.txt', 'a') as file:
                    file.write(f"{message}\n")
    fenetre_flechettes.blit(cible_arriere_plan, (0, 0))  # Affichage de l'image d'arrière-plan
    display.flip()
