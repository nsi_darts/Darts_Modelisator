__**DART MODELISATOR**__

Participants aux projets : 
- MBA MEZUI Dream-Benn
- FROEHLICH Alexandre

Description globale du projet : 

A la suite d'un souci sur notre ancien projet, nous en avons discuté avec notre équipe pour changer complètement de système de modélisation. Nous avons eu idée alors d'un système de jeu de fléchettes ou nous allons modéliser une fléchette allant vers une cible et récupérer ainsi les points remportés 

Programmes du projet : 

- Le fichier Flechettes.py est le fichier principal, il permet l'affichage de la cible ainsi que la récupération des coordonnées de la souris ainsi que la couleur
- Le fichier Points_Cibles.py relié au fichier Flechettes.py permet de récupérer la couleur qu'on a cliqué avec la souris et de récupérer le nombre de points correspondant. Si la fléchette est en dehors de la cible, alors ca renvoit 0 points
- Le fichier Deviation.py représente les différents facteurs présents afin de changer le comportement de la flèche. Nous avons eu l'idée d'intégrer les différents paramètres suivant : La distance entre vous et la cible, la force qu'on exerce pour lancer la fléchette et l'angle de visée en degrés. Le programme va du coup calculer les coordonnées de déviation pour les intégrer facilement dans le fichier Flechettes.py 
Le module utilisé est **PyGame**

Répartition des tâches : 

- Dream-Benn :
Dream-Benn c'est occupé à trouver une image coéerante pour la cible. Il faut savoir que l'étape de la recherche a été longue, car il nous fallait une image ou lorsqu'on allait appliquer une couleur que la cible ne se métamophorise pas entre-temps.
Il c'est aussi occupé à choisir les différentes couleurs pour la cible et a participer au développement de la récupération de la couleur
Il c'est aussi occupé du système de déviation en recherchant les différentes équations pour modéliser la trajectoire de la fléchette.

- Alexandre :
Alexandre a participé à environ 70% du codage du programme. Il a pour récupérer les RVB de chaque couleur rajouter une commande pour les inscrire dans un fichier .txt. Ensuite, il a fais le fichier Points_Cible.py car les RVB devaient être noté à la main (aide de Dream-Benn)
Mais également, il a participé a l'amélioration du système de la modélisation

- Autres informations sur le projet :

Le projet est entièrement fait en Python, la version actuelle du projet est en 3.3.0 (auparavant en 3.0.7 mais un bug au niveau de la récupération des coordonnées a pris beaucoup de temps)
L'utilisation de Internet tel que des forums de développement ont été utilisés ainsi que la documentation Pygame et un outil de redimension d'image pour notre cible.

- Bugs actuels et non correctables pour l'instant :

La cible n'est pas entièrement parfaite, certaines zones peuvent valoir aucun point ! (A voir si je peux l'améliorer !)
La déviation n'est pas toujours parfaite, en effet vous ne pouvez pas savoir a l'avance ou la flèche va atterir. Cela cause parfois un rejet de point et que vous ayez systématiquement presque dans 80% des cas 0 points ! Une plage de valeur pourra être faite si il nous reste un peu de temps. (Bon actuellement, je vais rien changer mais vous pouvez essayer de mettre des valeurs aléatoires, le code de toute manière ne marchera pas si les coordonnées dépassent la fenêtre :)

Dernière mise à jour : 
- Date : 11 Mars 2024 à 13:09
- Description : Réglage du système de déviation dans Deviation.py ainsi que l'ajout d'une îcone de la fenêtre Pygame. A venir : Ajout d'un point sur la cible pour savoir ou la flèche a atterri !
  
Pour les trophées NSI 2024 !! 🏆
